import RPi.GPIO as GPIO
from time import sleep


def led(sek):
    GPIO.setmode(GPIO.BCM)
    # Pin Definitons:
    ledPin = 18 # Broadcom pin 23 (P1 pin 16)
    GPIO.setup(ledPin, GPIO.OUT)

    GPIO.output(ledPin, GPIO.HIGH)
    sleep(sek)
    GPIO.output(ledPin,GPIO.LOW)