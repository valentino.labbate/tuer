path_user="/home/pi/Desktop/Git/Tuersteuerung/speicheruser.txt"
path_pass="/home/pi/Desktop/Git/Tuersteuerung/speicherpass.txt"

def schreiben_user(o):
    datei = open(path_user,"r+")
    datei.write(o)
    datei.close()
    
def schreiben_pass(o):
    datei = open(path_pass,"r+")
    datei.write(o)
    datei.close()
    
def lesen_pass():
    datei = open(path_pass, "r+")
    woerter = datei.read()
    datei.close()
    return woerter  
    
    
def lesen_user():
    datei = open(path_user, "r+")
    woerter = datei.read()
    datei.close()
    return woerter


def loeschen_user():
    with open(path_user, "r+") as f:
        f.truncate()

def loeschen_pass():
    with open(path_pass, "r+") as f:
        f.truncate()
    




