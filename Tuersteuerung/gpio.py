import RPi.GPIO as GPIO
from time import sleep


def beep(n):
    GPIO.setmode(GPIO.BCM)
    servoPin=14
    GPIO.setup(servoPin,GPIO.OUT)

    p = GPIO.PWM(servoPin, 50)
    p.start(0)

    l = [2.5,5,7.5,10,12.5,15,17.5,20]


    GPIO.output(servoPin, True)
    p.ChangeDutyCycle(1)
    sleep(1)
    p.stop()
    
    sleep(n)
    p.start(servoPin)
    p.ChangeDutyCycle(1)
    #p.ChangeDutyCycle(2)
    sleep(1)
    p.stop()
    #p.ChangeDutyCycle(0)
    GPIO.output(servoPin, False)

