#source tuer/bin/activate
from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort,send_file , url_for, send_from_directory
from time import sleep
import os
import sav as speichern
import gpio
import socket
import netz
import led 
from datetime import datetime



app = Flask(__name__)
app.secret_key = os.urandom(12)
app.config["ENVIRONMENT"] = "development"
app.config["FLASK_APP"] = "tuersteuerung"
app.config["SECRET_KEY"] = os.urandom(12)
app.config['DOWNLOAD_FOLDER'] = "/home/pi/Desktop/Git/Tuersteuerung/Downloads/"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///user.db'
app.config['ALLOWED_EXTENSIONS'] = ['.apk']


@app.route("/index")
@app.route("/")
def home():
    return render_template("index.html")
    
@app.route("/steuerung", methods=["GET", "POST"])
def steuerung():
    if not session.get('logged_in'):
        return render_template("login.html")
    else:
        return render_template("steuerung.html")
    
@app.route("/steuerung/<sek>", methods=["POST","GET"])
def oeffnen(sek):
    if not session.get('logged_in'):
        return render_template("login.html")
    else:
        n = int(sek)
        led.led(2)
        gpio.beep(n)
        
        print(sek + "Sekunden")
        return render_template("steuerung.html")

@app.route("/steuerung/", methods=["POST","GET"])
def oeffnen2():
    if not session.get('logged_in'):
        return render_template("login.html")
    else:
        sek = request.args.get('slider')
        return redirect(url_for("oeffnen",sek=sek))

@app.route("/downloads")
def download():
    return render_template("downloads.html")

@app.route("/downloads/", methods=["GET", "POST"])
def downloads():
    print(url_for('static', filename="Tuer.apk"))
    return send_from_directory("static", filename="Tuer.apk")

@app.route("/login", methods=["POST"])
def do_admin_login():
    print(speichern.lesen_pass())
    print(speichern.lesen_user())
    if request.form['password'] == speichern.lesen_pass() and request.form['username'] == speichern.lesen_user():
        nutzername = request.form['username']
        session['logged_in'] = True
    else:
        flash('Falsches Passwort')
    return home()

@app.route("/profil", methods=["GET"])
def profil():#Form zum PW und User ändern
    if not session.get('logged_in'):
        return render_template("login.html")
    else:
        return render_template("profil.html")

@app.route("/pw", methods=["POST"])
def options():#Pw und Nutzernamen ändern
    
    speichern.loeschen_pass()
    speichern.schreiben_pass(request.form['password'])
    speichern.loeschen_user()
    speichern.schreiben_user(request.form['username'])
    return render_template("login.html")

@app.route("/netzwerkoptionen")
def netoptions():
    return render_template("netzwerk.html")

@app.route("/netzwerk", methods=["POST"])
def netzwerk():
    name = request.form['name']
    SSID = request.form['ssid']
    password = request.form['passwort']
    msg = "Verbindet mit folgendem Netzwerk:" + name , SSID , password
    netz.createNewConnection(name, SSID, password)
    return render_template("msg.html", msg=msg)


@app.route("/msg/<text>")
def msg(text):#Eine Seite die eine übertragene Msg anzeigt
    return render_template("msg.html", msg=text)


@app.route("/logout")
def logout():#lässt den User ausloggen
        session["logged_in"] = False
        return home()

@app.route("/off")
def off():#Fährt den Computer herunter
    if not session.get('logged_in'):
        return render_template("login.html")
    else:
        os.system("poweroff") 
        exit()

@app.route("/reboot")
def reboot():#rebootet den Computer
    if not session.get('logged_in'):
        return render_template("login.html")
    else:
        os.system('reboot')
        exit()

@app.route("/beenden")
def beenden():
    if not session.get('logged_in'):
        return render_template("login.html")
    else:
        exit()
        return home()

@app.route("/monitor")
def monitor():
    total_memory, used_memory, free_memory = map(
    int, os.popen('free -t -m').readlines()[-1].split()[1:])
    ram = "RAM memory % used:", round((used_memory/total_memory) * 100, 2)
    print(ram)
    IPAdresse = os.popen('hostname -I').readlines()
    cpu = "Ihre IP-Adresse lautet " + " ".join(IPAdresse) + "."

    return render_template("monitor.html", ram=ram, cpu=cpu)

if __name__ == "__main__":
    app.run(host="0.0.0.0",port=5000, debug=True)
      
#flask run -h 0.0.0.0 --debug

